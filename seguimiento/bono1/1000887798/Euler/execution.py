from euler import edo   
#NOTA 5
if __name__=="__main__":
    
    print("Solucionando")
    #Definir entradas

    a,b,n = 0,4,20
    y0 = 1
    punto = 1.8
    
    #Definir la función
    f = lambda x,y: 5*y/(x+1)

    #Instanciar la clase
    soledo=edo(a,b,n,y0,f)
    #Usar método de la clase
    soledo.figPLot()

    # print(f"h= {soledo.h():.2f}")
    # print(f"x= {soledo.X()}")
    # print(f"y= {soledo.euler()}")
    # print(f"sol analitica= {soledo.solanali()}")
    # print(f"sol RK4= {soledo.RK_4()}")

    print("Listo!")

