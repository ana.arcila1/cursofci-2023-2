
from solEDO import ecuacion
import matplotlib.pyplot as plt
#NOTA 5
if __name__ == "__main__":

    y4 = ecuacion(0, 1, 10000)
    print("\n\ny(4):\nEuler: {}\nRK: {}\nAnalitica: {}".format(y4.Euler()[1][-1], y4.RK()[1][-1], y4.solucionAnalitica().y[0][-1]))
    y4.figPlot()

    y1_1 = ecuacion(0, 1, 100, 1.1)
    print("\n\ny(1.1):\nEuler: {}\nRK: {}\nAnalitica: {}".format(y1_1.Euler()[1][-1], y1_1.RK()[1][-1], y1_1.solucionAnalitica().y[0][-1]))
    y1_1.figPlot()

