
from problema import AnimatedHeatDiffusionSimulation
from problema import HeatDiffusionSimulation
import numpy as np

T0=80 # Temperatura inicial (debe ser positiva).
grid_size = 30 # Tamaño de la cuadrícula.
initial_temperatures = np.zeros(grid_size * grid_size)
initial_temperatures[grid_size//4:grid_size//2] = T0
iteraciones = 100 # Número de iteraciones o tiempo final.

if __name__ == "__main__":
    simulation = AnimatedHeatDiffusionSimulation(initial_temperatures, iteraciones, grid_size)
    simulation.animate_simulation(fps=5)
