import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

class pozoTemporal:
    def __init__(self, m:float ,L:float,estados:tuple=(1,2),iteraciones: int=100000,\
                 hbar:float=1,pasoTiempo:float=0.01,tmax:float=1):
        """
        Esta clase calcula la función de onda de cualquier conjunto
        de estados superpuestos de un pozo infinito, y devuelve una animación
        que muestra su evolución temporal. Para verificar la validez de la función
        de onda se calcula la integral de su módulo cuadrado mediante montecarlo.

        Algunas funciones tienen la variable mc=False, esta es para generar los
        respectivos arrays con un x extenso o predefinido con el propósito de 
        graficar.
        """

        self.N=len(estados)
        self.states=estados
        self.L=L
        self.m=m
        self.hbar=hbar
        self.NN=iteraciones
        self.t=np.arange(0,tmax,pasoTiempo)


        
    def x_(self,mc=False):
        """
        Devuelve un array de x en el intervalo [0,L]
        """
        if mc==True:
            self.x=np.linspace(0,self.L,self.NN)
            # self.x = np.sort(np.random.uniform(0,self.L,self.NN))
        else:
            self.x = np.linspace(0,self.L,len(self.t))
    
    def phi(self,n,mc=False):
        """
        Devuelve la n-ésima función de onda de la partícula
        """
        self.x_(mc)
        return np.sqrt(2/self.L)*np.sin(np.pi*n*self.x/self.L)
    
    def E(self,n):
        """
        Devuelve la n-ésima energía de la partícula
        """
        return (n**2*np.pi**2*self.hbar**2)/(2*self.m*self.L**2)

    
    def phiT(self,n,t,mc=False):
        """
        Devuelve la componente n-ésima de la función de onda de la partícula en función del tiempo
        """
        return self.phi(n,mc) * np.exp( -1j * self.E(n) *t/self.hbar )
    
    def psi(self,t,mc=False):
        """
        Devuelve la función de onda de la partícula en función del tiempo
        """
        psi=0
        for i in self.states:
            psi += self.phiT(i,t,mc) # Suma los estados solicitados uno por uno 
        psi=psi/np.sqrt(self.N) # Normaliza
        return psi
    
    def ymax(self,t):
        """
        Devuelve la altura máxima del módulo cuadrado de una función de onda en t
        """

        y_=abs(self.psi(t))**2
        return max(y_)

    def probMC(self,t,min=0,max=None):
        """
        Calcula la probabilidad de encontrar la partícula entre min y max
        si no se ingresan valores se calcula en todo el dominio
        """
        if max==None:
            max=self.L

        psi_t = abs(self.psi(t,True))**2 # Calcula el mod cuadrado con un array de x aleatorio
        ymax=self.ymax(t) # Encuentra su máximo
        A= ymax * (max-min) # El área del rectángulo donde está la función

        yrand=np.random.uniform(0,ymax,self.NN) # Genera alturas aleatorias 
        debajo=len(yrand[ yrand<psi_t])/self.NN # Cuenta los puntos donde la
                                                # altura fue menor y divide entre el total
        return debajo*A
    
    def animplot(self,fps=30):
        """
        Crea una animación de la probabilidad de encontrar la partícula en la posición x
        Ajustar los fps para mejor visualización.
        Puede que algunos estados sean demasiado energéticos para visualizarse correctamente,
        ahí hay que disminuir el valor pasoTiempo de la clase
        """    
        print('Graficando...')
        def init():
            line.set_data([], [])
            return line,
        
        def animate(frame):
            self.x_() # Define el x para graficar
            y = abs(self.psi(frame))**2 # Calcula el mod^2 en cierto t

            line.set_data(self.x, y) #Graficar esto y blablabla
            ax.set_xlabel('Posición')
            ax.set_ylabel('$|\hspace{0.1} \psi (x)\hspace{0.1}|^2$')
            ax.set_title(f'$\int |\psi|^2 dx$ por Montecarlo = {self.probMC(frame):.3f} con {self.NN} iteraciones')
            ax.legend([f'n = {self.states}\nt = {frame:.3f} s'])
            return line,
        
        limsup=max(abs( self.psi(self.t) )**2)+1 # Límite superior mirando Todos los tiempos

        fig = plt.figure()
        ax = plt.axes(xlim=(0, self.L), ylim=(0,limsup))
        line, = ax.plot([], [], lw=2)

        anim = FuncAnimation(fig, animate, init_func=init,
                            frames=self.t, interval=20, blit=True)
        anim.save('pozoTemporal.gif', fps=fps)
        print('pozoTemporal.gif')



