Para instalar la librería:

pip install allkernAna

Y a continuación se pone un ejemplo de cómo utilizarlo:

path = 'Colombia_COVID19_Coronavirus_casos_diarios.csv'
h = 10

from allkernAna.modulo import suavizado

a1 = suavizado(path, h)
a1.figPlot()

En donde h es un coeficiente de suavizado y path es un string con
el camino al documento.